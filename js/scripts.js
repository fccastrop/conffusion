$(document).ready(function(){
    $("#mycarousel").carousel( { interval: 2000 } );
    $("#carouselButton").click(function(){
        if ($("#carouselButton").children("span").hasClass('fa-pause')) {
            $("#mycarousel").carousel('pause');
            $("#carouselButton").children("span").removeClass('fa-pause');
            $("#carouselButton").children("span").addClass('fa-play');
        }
        else if ($("#carouselButton").children("span").hasClass('fa-play')){
            $("#mycarousel").carousel('cycle');
            $("#carouselButton").children("span").removeClass('fa-play');
            $("#carouselButton").children("span").addClass('fa-pause');                    
        }
    });
    $("#ReserveATableButton").click(function() {
        $("#ReservATable").modal();
    });
    $("#cancelReserveButton").click(function(){
        $("#ReservATable").modal("toggle");      
    });
    $("#quitReserveButton").click(function(){
        $("#ReservATable").modal("toggle");      
    });
    $("#loginButton").click(function() {
        $("#loginModal").modal();
    });
    $("#cancelLoginButton").click(function(){
        $("#loginModal").modal("toggle");      
    });
    $("#quitLoginButton").click(function(){
        $("#loginModal").modal("toggle");      
    });
});